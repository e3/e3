.. figure:: docs/img/logo-slim.png

==
e3
==

e3 is the EPICS environment developed at the European Spallation Source ERIC.
This consists of a set of tools to build, package, and load EPICS modules and
IOCs in a custom way in order to facilitate maintenance of the ESS site over
its lifetime.

For a project history, see the `changelog`_.

-------------
Documentation
-------------

Documentation is hosted using `GitLab Pages`_.

------------
Introduction
------------

This repository contains a set of tools to help build and maintain one or more
e3 environments. In particular, it contains tools to build and deploy EPICS
base, require, as well as certain curated collections of EPICS modules into
that environment.

Prerequisites
=============

e3 is primarily built for CentOS 7, but should also work for Debian and Ubuntu.
There are quite a few packages that need to be installed in order to install
the full compliment of e3 modules. For the automated builds, we use a `Docker
image`_.

Installing
==========

.. code-block:: console

  $ pip3 install --user e3 -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
  $ e3-build -h

Usage
=====

Building an EPICS environment
-----------------------------

Select your specification of choice from the `specifications repository`_ and
use it with ``e3-build``.

Loading and using an EPICS environment
--------------------------------------

You load an EPICS environment in a shell by sourcing a file called
``activate`` in your installed directory tree:

.. code-block:: console

  $ source /opt/epics/base-7.0.7/require/5.0.0/bin/activate

You can then finally load an IOC shell by running ``iocsh <args>``.

-----------------------
Current e3 installation
-----------------------

We have a regularly updated interactive `module viewer`_ which allows you to
see which modules are installed in which e3 environment on the shared file
system in use at ESS. This also allows you to see the dependencies between
modules, and in particular it lets you see just how central `asyn`_ is.

-------------
Issue tracker
-------------

We use JIRA to track known issues (see them `here`_), and we have a `service
desk`_ for reporting new ones - but you can also do so directly here on this
repository.

------------
Contributing
------------

Contributions through pull/merge requests only.

.. [1] ethercat-generic-dkms-1.5.2.ESS1-1 is an ESS internal package. It can be
  found at:
  <https://artifactory.esss.lu.se/artifactory/rpm-ics/centos/7/x86_64/>. For ESS
  internal users, this package can be installed the same way as installing
  standard CentOS packages. For external users, one will need to add this
  repository to package manager's repository-search-list to install this
  package.
.. _`changelog`: CHANGELOG.md
.. _`GitLab Pages`: http://e3.pages.esss.lu.se
.. _`Docker image`: https://gitlab.esss.lu.se/ics-docker/centos-e3
.. _`module viewer`: http://e3.pages.esss.lu.se/module-overview
.. _`specifications repository`: https://gitlab.esss.lu.se/e3/specifications
.. _`asyn`: https://gitlab.esss.lu.se/e3/wrappers/e3-asyn
.. _`here`: https://confluence.esss.lu.se/x/mI-IFQ
.. _`service desk`: https://jira.esss.lu.se/plugins/servlet/desk/portal/40
.. _`FAQ`: https://confluence.esss.lu.se/x/VZX7F
