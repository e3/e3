from pathlib import Path
from unittest import mock

from e3.fs.make import find_cxx_stds, find_undefined_vars, parse_file_for_cxx_stds

CXXFLAGS_WITH_STD = "USR_CXXFLAGS += -ggdb -std=c++11 -O0"
CXXFLAGS_WITHOUT_STD = "USR_CXXFLAGS = -g -O1"


def test_cxx_stds():
    assert parse_file_for_cxx_stds([CXXFLAGS_WITH_STD])
    assert not parse_file_for_cxx_stds([CXXFLAGS_WITHOUT_STD])
    assert not parse_file_for_cxx_stds(["#" + CXXFLAGS_WITH_STD])


def test_find_cxx_stds(tmpdir):
    p = Path(tmpdir)
    fake_makefile = "tmp.Makefile"
    file = p / fake_makefile

    initial = "USR_CXXFLAGS += -std=c++11"
    expected = [(file, ["c++11"])]

    file.write_text(initial)
    with mock.patch.object(Path, "glob") as glob:
        glob.return_value = [file]
        actual = find_cxx_stds(p)

    assert actual == expected


def test_undefined_vars(tmpdir):
    defined_vars = set(["A", "D"])
    undefined_vars = set(["B", "C"])
    p = Path(tmpdir)
    with open(p / "tmp.Makefile", "w") as f:
        for var in defined_vars:
            f.write(f"{var} = {var}\n")
            f.write(f"SOURCES += $({var})\n")
        for var in undefined_vars:
            f.write(f"SOURCES += $({var})\n")

    vars = find_undefined_vars(p)
    assert vars == undefined_vars
