from pathlib import Path, PosixPath

import pytest

from e3.fs.environment import InstalledEnvironment
from e3.module import EPICSBaseSource, ModuleSource, RequireSource


@pytest.fixture
def environment():
    yield InstalledEnvironment(
        Path("/beefcafe"),
        base_version="base_version",
        require_version="require_version",
    )


class TestInstalledEnvironment:
    def test_ctor(self, environment):
        assert environment.root == PosixPath("/beefcafe")
        assert environment.base_version == "base_version"
        assert environment.require_version == "require_version"
        assert environment.base_directory == PosixPath("/beefcafe/base-base_version")
        assert environment.modules_directory == PosixPath(
            "/beefcafe/base-base_version/require/require_version/siteMods"
        )

    def test_repr(self, environment):
        assert (
            repr(environment)
            == "InstalledEnvironment(PosixPath('/beefcafe'), 'base_version', 'require_version')"
        )

    def test_has_is_false_when_environment_is_not_installed(self, environment):
        assert not environment.has("module", "version")

    @pytest.mark.parametrize(
        "type_",
        [
            EPICSBaseSource(version="base_version"),
            RequireSource(version="require_version"),
            ModuleSource("", versions=["module_version"]),
        ],
        ids=["base", "require", "module"],
    )
    def test_has_is_true_when_path_exists(self, tmp_path, type_):
        environment = InstalledEnvironment(tmp_path, "base_version", "require_version")
        environment.modules_directory.mkdir(parents=True)
        for version in type_.versions:
            if type(type_) is ModuleSource:
                install_dir = environment.modules_directory / type_.name / version
                install_dir.mkdir(parents=True)
                (install_dir / f"{type_.name}_meta.yaml").touch()
            elif type(type_) is RequireSource:
                (environment.require_directory / f"{type_.name}_meta.yaml").touch()

            assert environment.has(type_.name, version)
