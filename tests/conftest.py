from pathlib import Path

import pytest


@pytest.fixture
def test_data_directory():
    data_directory = Path(__file__).absolute().parent / "data"
    return data_directory
