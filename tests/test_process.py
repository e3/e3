from pathlib import Path
from unittest.mock import patch, MagicMock

import pytest

from e3.definition import BuildDefinition
from e3.exceptions import ProcessException
from e3.fs.environment import InstalledEnvironment
from e3.module import ModuleSource
from e3.process import EnvironmentBuildProcess, SpecificationGenerationProcess


@pytest.fixture
def build_process(tmp_path: Path):
    with patch("e3.process.WrapperRegistry", autospec=True):
        build_process = EnvironmentBuildProcess(
            specification=tmp_path,
            build_dir=tmp_path,
            install_path=tmp_path,
            use_ssh=False,
            token="",
            verbose=False,
            log_file=False,
            jobs=1,
        )
        build_process.registry.get_config_from.side_effect = (
            lambda name, ref, **kwargs: f"""
E3_BASE_VERSION := {name + ref}
E3_REQUIRE_VERSION := {name + ref}
"""
        )
        yield build_process


class TestEnvironmentBuildProcess:
    @patch("e3.fs.environment.InstalledEnvironment.has", return_value=True)
    def test_curate_removes_installed_modules(
        self, mock_has, build_process: EnvironmentBuildProcess
    ):
        module_installed = ModuleSource("foo", versions=["0.0.0+0"])
        test_environment = BuildDefinition(
            "target",
            base_ref="foo",
            require_ref="bar",
            modules={"foo": module_installed},
        )
        for module in test_environment.modules:
            for version in test_environment.modules[module].versions:
                test_environment.modules[module].versions[version][
                    "version_string"
                ] = version
        build_process.environment = test_environment
        build_process.target_environment = InstalledEnvironment(
            build_process.install_path, "foo", "bar"
        )

        assert all(
            module.versions for module in build_process.environment.modules.values()
        )

        build_process.curate()
        assert not any(
            module.versions for module in build_process.environment.modules.values()
        )

    def test_fetch_data_populates_installed_environment(
        self, build_process: EnvironmentBuildProcess
    ):
        test_environment = BuildDefinition("target", base_ref="foo", require_ref="bar")
        build_process.environment = test_environment

        build_process.fetch()

        assert build_process.target_environment.base_version == "basefoo"
        assert build_process.target_environment.require_version == "requirebar"

    def test_fetch_data_populates_dep_info(
        self, build_process: EnvironmentBuildProcess
    ):
        test_environment = BuildDefinition("target", base_ref="foo", require_ref="bar")
        build_process.environment = test_environment

        build_process.fetch()

        assert build_process.environment.modules.pop("require").dependencies == {"base"}

    @patch("e3.process.Builder.build", return_value=([], [("qux", "baz")]))
    def test_build_environment_failure_raises_exception(
        self, mock_build, build_process: EnvironmentBuildProcess
    ):
        test_environment = BuildDefinition("target", base_ref="foo", require_ref="bar")
        build_process.environment = test_environment
        build_process.target_environment = InstalledEnvironment(
            build_process.install_path, "foo", "bar"
        )

        build_process.resolve()

        with pytest.raises(ProcessException):
            build_process.build()

    @patch("e3.process.Builder.build")
    def test_successful_builds_are_logged(
        self, mock_build, caplog, build_process: EnvironmentBuildProcess
    ):
        # Return all builds as successful
        mock_build.side_effect = lambda m, _: ([(m.name, v) for v in m.versions], [])

        base_version = "foo"
        require_version = "bar"
        test_environment = BuildDefinition(
            "target", base_ref=base_version, require_ref=require_version
        )
        build_process.environment = test_environment
        build_process.target_environment = InstalledEnvironment(
            build_process.install_path, base_version, require_version
        )

        build_process.resolve()
        build_process.build()

        assert "Build successful." in caplog.text
        assert f"Module base, {base_version}" in caplog.text
        assert f"Module require, {require_version}" in caplog.text


@pytest.fixture
def specification_process(tmp_path: Path):
    with patch("e3.process.WrapperRegistry", autospec=True):
        specification_process = SpecificationGenerationProcess(
            formula=None,
            build_dir=tmp_path,
            install_path=tmp_path,
            use_ssh=False,
            group_id=0,
            token="",
            verbose=False,
            log_file=None,
            branch=None,
        )
        mod_ver = "1.0.0+0"
        specification_process.registry.get_config_from.return_value = f"""
E3_BASE_VERSION:=foo
E3_REQUIRE_VERSION:=bar
E3_MODULE_VERSION:={mod_ver}
"""
        specification_process.registry.read_version_from.return_value = mod_ver
        specification_process.registry.get_project.return_value.default_branch = "main"

        yield specification_process


class TestSpecificationGenerationProcess:
    def test_curate_updates_substitutions(
        self,
        specification_process: SpecificationGenerationProcess,
        test_data_directory,
    ):
        specification_process.formula = test_data_directory / "dummy_formula.yml"
        specification_process.setup()
        specification_process.fetch()

        specification_process.curate()
        assert specification_process.environment._formula["modules"]["baz"][
            "substitutions"
        ]

    def test_setup_formula_does_not_exists(
        self, specification_process: SpecificationGenerationProcess, test_data_directory
    ):
        specification_process.formula = "invalid-path"

        with pytest.raises(ProcessException):
            specification_process.setup()

    @patch("e3.process.Builder.build")
    def test_successful_builds_are_logged(
        self,
        mock_build,
        caplog,
        specification_process: SpecificationGenerationProcess,
        test_data_directory,
    ):
        mock_build.side_effect = lambda m, _, with_substitutions=None: (
            [(m.name, v) for v in m.versions],
            [],
        )
        specification_process.formula = test_data_directory / "dummy_formula.yml"
        specification_process.setup()
        specification_process.fetch()
        specification_process.curate()
        specification_process.resolve()
        specification_process.build()

        assert "Build successful." in caplog.text
        assert "Module base, foo" in caplog.text
        assert "Module require, bar" in caplog.text
        assert "Module baz, main" in caplog.text

    @patch("e3.process.Builder.build", return_value=([], [("qux", "baz")]))
    def test_specification_build_environment_failure_raises_exception(
        self,
        mock_build,
        specification_process: SpecificationGenerationProcess,
        test_data_directory,
    ):
        specification_process.formula = test_data_directory / "dummy_formula.yml"
        specification_process.setup()
        specification_process.fetch()
        specification_process.curate()
        specification_process.resolve()

        with pytest.raises(ProcessException):
            specification_process.build()

    @patch("e3.process.SpecificationGenerationProcess._update_module")
    @patch(
        "e3.process.SpecificationGenerationProcess._gather_project_submodule_changes"
    )
    def test_specification_generate_method(
        self,
        mock_update_module,
        mock_submodule_changes,
        caplog,
        specification_process: SpecificationGenerationProcess,
        test_data_directory,
    ):
        specification_process.formula = test_data_directory / "dummy_formula.yml"
        specification_process.setup()
        specification_process.fetch()
        specification_process.curate()
        specification_process.resolve()

        mock_submodule_changes.return_value = MagicMock()
        specification_process.generate(dry_run=False)

        assert "Generating specification..." in caplog.text

    @patch("e3.process.SpecificationGenerationProcess._update_module")
    def test_specification_generate_method_failed_on_update_module(
        self,
        mock_update_module,
        specification_process: SpecificationGenerationProcess,
        test_data_directory,
    ):
        specification_process.formula = test_data_directory / "dummy_formula.yml"
        specification_process.setup()
        specification_process.fetch()
        specification_process.curate()
        specification_process.resolve()

        mock_update_module.side_effect = ValueError("")
        with pytest.raises(ProcessException):
            specification_process.generate(dry_run=False)

    @patch("e3.process.pathlib.Path.iterdir")
    @patch("e3.process.pathlib.Path.rmdir")
    def test_teardown(
        self,
        mock_iterdir_path,
        mock_rm_path,
        specification_process: SpecificationGenerationProcess,
        test_data_directory,
    ):
        specification_process.formula = test_data_directory / "dummy_formula.yml"
        specification_process.setup()
        specification_process.fetch()

        mock_iterdir_path.return_value = MagicMock()
        mock_rm_path.return_value = MagicMock()
        specification_process.teardown()

        mock_iterdir_path.assert_called_once()
        mock_rm_path.assert_called_once()
