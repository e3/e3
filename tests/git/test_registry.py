from unittest.mock import patch

import pytest

from e3.git.registry import WrapperRegistry


class MockProject:
    def __init__(self):
        self.ssh_url_to_repo = "www"


@patch("e3.git.registry.WrapperRegistry.get_project", return_value=MockProject())
@patch(
    "e3.git.registry.WrapperRegistry.get_all_wrapper_projects",
    return_value={"e3-fake_module": 0},
)
def test_registry(mock_get_all_wrapper_projects, mock_get_project):
    registry = WrapperRegistry()
    assert registry.get_project_id("fake_module") == 0

    assert registry.get_project().ssh_url_to_repo == "www"
    with pytest.raises(KeyError):
        registry.get_project_id("")
