from unittest.mock import patch

import pytest
from pytest import raises

from e3.git.tag.remote import generate_remote_tag
from e3.exceptions import InvalidVersionException
from e3.git.registry import WrapperRegistry
from e3.git.tag.remote import read_remote_versions


@pytest.fixture(scope="module", autouse=True)
def mock_commit_hash():
    with patch(
        "e3.git.registry.WrapperRegistry.get_commit_hash", return_value="cafe0123456789"
    ) as fixture:
        yield fixture


def mock_gitlab_read_version_from(
    self,
    name: str,
    ref: str,
    *,
    config: str,
    var: str,
    validate: bool = True,
) -> str:
    if config == "configure/RELEASE":
        if var == "EPICS_BASE":
            return "/epics/base-7.0.6.1"
        else:
            return "4.0.0"
    versions = {
        "base": "7.0.6.1",
        "invalid-version-letter": "a.2.3+0",
        "invalid-version-makefile-variable": "${E3_BASE_VERSION}",
    }
    return versions.get(name, "1.2.3+0")


@pytest.mark.parametrize(
    "module, expected",
    [
        ("module", "7.0.6.1-4.0.0/1.2.3+0-cafe012"),
        ("base", "7.0.6.1-NA/7.0.6.1-cafe012"),
        ("require", "7.0.6.1-4.0.0/4.0.0-cafe012"),
    ],
)
@patch(
    "e3.git.registry.WrapperRegistry.read_version_from",
    mock_gitlab_read_version_from,
)
@patch("e3.git.registry.WrapperRegistry.get_all_wrapper_projects", return_value={})
def test_generate_remote_tag(mock_get_all_wrapper_projects, module, expected):
    registry = WrapperRegistry()

    actual = generate_remote_tag(registry=registry, module=module, ref="ref")
    assert actual.startswith(expected)


@pytest.mark.parametrize(
    "module",
    [
        "invalid-version-letter",
        "invalid-version-makefile-variable",
    ],
)
@patch(
    "e3.git.registry.WrapperRegistry.read_version_from",
    mock_gitlab_read_version_from,
)
@patch("e3.git.registry.WrapperRegistry.get_all_wrapper_projects", return_value={})
def test_read_local_versions_raises_exceptions_for_invalid_tags(
    mock_get_all_wrapper_projects, module
):
    registry = WrapperRegistry()
    with raises(InvalidVersionException):
        _ = read_remote_versions(registry=registry, module=module, ref="ref")


@pytest.mark.parametrize(
    "module, version",
    [("base", "1.2.3.4.5"), ("base", "foo")],
)
@patch("e3.git.registry.WrapperRegistry.read_version_from")
@patch(
    "e3.git.registry.WrapperRegistry.get_all_wrapper_projects",
    return_value={"base": 0, "require": 1},
)
def test_invalid_base_versions(
    mock_get_all_wrapper_projects,
    mock_read_version_from,
    module: str,
    version: str,
):
    registry = WrapperRegistry()
    mock_read_version_from.return_value = version
    with raises(InvalidVersionException):
        _ = read_remote_versions(registry=registry, module=module, ref="ref")
