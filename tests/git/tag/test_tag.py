from e3.git.tag import Tag


class TestTag:
    def test_from_components(self):
        tag = Tag.from_components(
            base_ver="foo", require_ver="bar", module_ver="baz", commit_sha="qux"
        )
        assert tag.name.startswith("foo-bar/baz-qux")
