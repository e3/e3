import functools
import pathlib
from unittest.mock import patch

import pytest
from git import Repo

from e3 import utils
from e3.utils import WrapperType, get_wrapper_type, modify_config_file


class TestWrapperType:
    @pytest.mark.parametrize(
        "name, _type",
        [
            ("base", WrapperType.BASE),
            ("require", WrapperType.REQUIRE),
            ("other", WrapperType.MODULE),
        ],
        ids=["base", "require", "other"],
    )
    def test_valid_name(self, name, _type, tmpdir):
        base_path = pathlib.Path(tmpdir) / f"e3-{name}"
        base_path.mkdir()

        assert get_wrapper_type(base_path) == _type

    def test_invalid_name_raises_typeerror(self):
        invalid_path = pathlib.Path("/invalid/path/to/file")
        with pytest.raises(TypeError):
            get_wrapper_type(invalid_path)

    @patch("git.repo.fun.is_git_dir", return_value=True)
    def test_wrapper_type_if_repo(self, mock_is_git_dir, tmpdir):
        path = pathlib.Path(tmpdir) / "e3-foo"
        path.mkdir()
        repo = Repo(path)

        assert get_wrapper_type(repo) == WrapperType.MODULE


def test_read_makefile_definitions():
    string = """\
foo = bar  # a comment
baz := qux
test?=test
# key = should-not-be-registered
"""

    expected = {
        "foo": "bar",
        "baz": "qux",
        "test": "test",
    }
    actual = utils.read_makefile_definitions(string)
    assert actual == expected


def test_modify_makefile_definitions_with_matched_substitutions():
    initial = "foo := bar"
    substitutions = {"foo": "new_value"}

    expected = "foo := new_value"
    actual = utils.modify_makefile_definitions(initial, substitutions)
    assert actual == expected


def test_modify_makefile_definitions_with_comments_present():
    initial = """\
foo := bar  # ignore
# foo = unchanged
"""
    substitutions = {"foo": "new_value"}

    expected = """\
foo := new_value  # ignore
# foo = unchanged
"""
    actual = utils.modify_makefile_definitions(initial, substitutions)
    assert actual == expected


def test_modify_makefile_definitions_with_multiple_matched_substitutions():
    initial = """\
foo := bar
baz = qux
"""
    substitutions = {"foo": "new_value", "baz": "another_val"}

    expected = """\
foo := new_value
baz = another_val
"""
    actual = utils.modify_makefile_definitions(initial, substitutions)
    assert actual == expected


def test_modify_makefile_definitions_with_unmatched_substitutions_returns_initial_content():
    initial = "foo := bar"
    substitutions = {"bar": "new_value"}

    expected = initial
    actual = utils.modify_makefile_definitions(initial, substitutions)
    assert actual == expected


def test_modify_config_file(test_data_directory):
    config_path = test_data_directory / "CONFIG"
    modify_config_file(config_path, "FOO", "bar")  # will change from "bar" to "bar"


def test_modify_config_file_raises_exception_if_file_is_missing(test_data_directory):
    incorrect_config_path = test_data_directory / "not-a-file"
    with pytest.raises(FileNotFoundError):
        modify_config_file(incorrect_config_path, "FOO", "bar")


@pytest.mark.parametrize(
    "initial, expected",
    [
        ("1.0.0+0", "1.0.0+0"),
        ("1.0.0", "1.0.0+0"),
    ],
)
def test_ensure_build_number(initial, expected):
    actual = utils.ensure_build_number(initial)
    assert actual == expected


@pytest.mark.parametrize(
    "initial, expected",
    [
        ("1.0.0+0", "1.0.0+1"),
        ("1.0.0", "1.0.0+1"),
        ("1.0.0-test+0", "1.0.0-test+1"),
        ("1.0.0-test", "1.0.0-test"),
        ("test", "test"),
    ],
)
def test_increment_build_number(initial, expected):
    actual = utils.increment_build_number(initial)
    assert actual == expected


@pytest.mark.parametrize(
    "initial, expected",
    [
        ("/epics/base-7.0.6.1", "7.0.6.1"),
        ("/epics/base-7.0.6.1-test", "7.0.6.1-test"),
    ],
)
def test_extract_base_version_if_valid_path(initial, expected):
    actual = utils.extract_base_version(initial)
    assert actual == expected


@pytest.mark.parametrize(
    "path",
    [
        "/invalid/path",
        "/epics/base-",
        "7.0.6.1",
    ],
)
def test_extract_base_version_raises_exception_if_not_valid_path(path):
    with pytest.raises(ValueError):
        utils.extract_base_version(path)


@pytest.mark.parametrize(
    "insert, expected",
    [
        (
            (
                {
                    "foo": {
                        "bar": "baz",
                    }
                },
                {
                    "foo": {
                        "qux": "",
                    }
                },
            ),
            {
                "foo": {
                    "bar": "baz",
                    "qux": "",
                }
            },
        ),
        (({"foo": "bar"}, {"foo": "qux"}), {"foo": "bar"}),
    ],
    ids=["with_two_nested_dicts", "with_same_key_in_two_dicts"],
)
def test_deep_merge(insert, expected):
    actual = utils.deep_merge(*insert)
    assert actual == expected


def test_deep_merge_with_three_nested_dicts():
    first = {
        "foo": {
            "bar": None,
        }
    }
    second = {
        "foo": {
            "baz": None,
        }
    }
    third = {
        "foo": {
            "qux": None,
        }
    }

    expected = {
        "foo": {
            "bar": None,
            "baz": None,
            "qux": None,
        }
    }
    actual = functools.reduce(utils.deep_merge, (first, second, third))
    assert actual == expected
