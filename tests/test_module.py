from pathlib import Path
from unittest.mock import patch

import pytest

from e3.builder import Builder
from e3.exceptions import FetchDataException, NotInitialisedError
from e3.git.registry import WrapperRegistry
from e3.module import EPICSBaseSource, ModuleSource, RequireSource


@pytest.fixture
def module_source():
    yield ModuleSource("fake", versions=[])


class MockProject:
    def __init__(self, url: str):
        self.http_url_to_repo = url


class TestModuleSource:
    def test_update_deps(self):
        config_content = {
            "FOO_DEP_VERSION": "1.2.3",
        }
        module = ModuleSource("", versions=["x"])
        module.set_config_data(config_content, "x")

        module.update_deps("x")
        assert "FOO".lower() in module.dependencies
        assert "FOO" not in module.dependencies

    def test_str(self, module_source):
        actual = str(module_source)
        expected = "fake"
        assert actual == expected

    def test_base_dependencies(self):
        b = EPICSBaseSource(version="")
        assert len(b.dependencies) == 0

    def test_require_dependencies(self):
        r = RequireSource(version="")
        assert len(r.dependencies) == 1
        assert "base" in r.dependencies

    @pytest.mark.parametrize(
        "_type",
        [
            EPICSBaseSource(version=None),
            RequireSource(version=None),
            ModuleSource("", versions=[]),
        ],
        ids=["base", "require", "module"],
    )
    def test_source_with_no_version(self, _type):
        _type

    @pytest.mark.parametrize(
        "module",
        [
            EPICSBaseSource(version=""),
            RequireSource(version=""),
            ModuleSource("", versions=[""]),
        ],
        ids=["base", "require", "module"],
    )
    def test_modify_install_path_overwrite_install_path(self, module, tmp_path):
        old_path = "/bar/baz"
        new_path = "/qux/quo"

        config_dir = tmp_path / "configure"
        config_dir.mkdir()

        install_config_path = tmp_path / module.install_config_file
        install_config_path.write_text(f"{module.install_config_var} = {old_path}")

        Builder.modify_install_path(
            module, repo_path=tmp_path, install_path=Path(new_path)
        )

        content = install_config_path.read_text()
        assert new_path in content
        assert old_path not in content

    def test_fetch_url_raises_exception_when_not_initialised(self, module_source):
        with pytest.raises(NotInitialisedError):
            module_source.project.http_url_to_repo

    @patch("e3.git.registry.WrapperRegistry.get_project")
    @patch("e3.git.registry.WrapperRegistry.get_all_projects")
    def test_url_is_set_after_fetch(
        self, mock_get_all_projects, mock_get_project, module_source
    ):
        mock_url = "https://foo.esss.lu.se"
        mock_get_project.return_value = MockProject(mock_url)

        r = WrapperRegistry()
        module_source._fetch_project(r)

        assert module_source.project.http_url_to_repo == mock_url

    @patch("e3.module.WrapperRegistry.get_all_projects", return_value={})
    def test_invalid_module_raises_exception(
        self, mock_get_all_projects, module_source
    ):
        registry = WrapperRegistry(address="https://foo", top_group=-1)
        with pytest.raises(FetchDataException):
            module_source._fetch_project(registry)
