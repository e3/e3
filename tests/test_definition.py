from pathlib import Path

import pytest

from e3.definition import BuildDefinition, Resolver, SpecificationDefinition
from e3.exceptions import DependencyResolutionError, ModuleExistsException
from e3.module import ModuleSource


@pytest.fixture
def build_definition():
    yield BuildDefinition("fake", base_ref="foo", require_ref="bar")


@pytest.fixture
def module_source():
    yield ModuleSource("fake")


class TestBuildDefinition:
    def test_create_environment(self):
        fake_module_list = {
            "qux": ModuleSource("qux", versions=[1, 2, 3]),
        }
        build_definition = BuildDefinition(
            "fake", base_ref="foo", require_ref="bar", modules=fake_module_list
        )

        assert build_definition.base_ref == "foo"
        assert build_definition.require_ref == "bar"
        assert "qux" in build_definition.modules
        assert len(build_definition.modules["qux"].versions) == 3
        assert 1 in build_definition.modules["qux"].versions

    def test_create_environment_from_specification(self, test_data_directory):
        build_definition = BuildDefinition.from_specification(
            test_data_directory / "dummy_specification.yml"
        )

        assert build_definition.base_ref == "foo"
        assert len(build_definition.modules) == 3  # base, require, and foo

    def test_create_environmentdefinition_from_formula_raises_exception(
        self, test_data_directory
    ):
        with pytest.raises(TypeError):
            _ = BuildDefinition.from_specification(
                test_data_directory / "minimal_formula.yml"
            )

    def test_create_specification_from_environment(self, build_definition, tmpdir):
        path = Path(tmpdir)
        specification_path = path / "spec.yml"
        build_definition.to_specification(specification_path)

    def test_add_module_to_specification(self, test_data_directory):
        build_definition = BuildDefinition.from_specification(
            test_data_directory / "dummy_specification.yml"
        )

        with pytest.raises(KeyError):
            assert build_definition.modules["bar"]

        build_definition.add_module(ModuleSource("bar", versions=["bar"]))
        assert build_definition.modules["bar"]

        with pytest.raises(ModuleExistsException):
            build_definition.add_module(ModuleSource("bar", versions=["bar"]))

    def test_remove_module_from_specification(self, test_data_directory):
        specification = BuildDefinition.from_specification(
            test_data_directory / "dummy_specification.yml"
        )

        assert "qux" in specification.modules

        specification.remove_module("qux")
        assert "qux" not in specification.modules


class TestSpecificationDefinition:
    @pytest.mark.parametrize(
        "initial, expected",
        [
            (
                {
                    "substitutions": {"file": {"foo": "global"}},
                    "modules": {"test": {"substitutions": {"file": {"foo": "module"}}}},
                },
                {
                    "file": {
                        "foo": "module",
                    },
                },
            ),
            (
                {
                    "substitutions": {"file_1": {"foo": "bar"}},
                    "modules": {"test": {"substitutions": {"file_2": {"baz": "qux"}}}},
                },
                {
                    "file_1": {
                        "foo": "bar",
                    },
                    "file_2": {
                        "baz": "qux",
                    },
                },
            ),
        ],
        ids=["same_file", "different_files"],
    )
    def test_combine_substitutions(self, initial, expected):
        specification_definition = SpecificationDefinition(
            "fake", formula=initial, base_ref="foo", require_ref="bar"
        )

        actual = specification_definition.combine_substitutions(name="test")
        assert actual == expected

    def test_global_substitutions_updated_by_module_versions(self):
        formula = {
            "modules": {
                "test": {
                    "substitutions": {
                        "configure/CONFIG_MODULE": {"E3_MODULE_VERSION": "1.2.3"}
                    }
                }
            }
        }
        specification_definition = SpecificationDefinition(
            "fake", formula=formula, base_ref="foo", require_ref="bar"
        )

        specification_definition.update_global_dependency_from_module("test")
        assert (
            specification_definition._formula["substitutions"][
                "configure/CONFIG_MODULE"
            ]["TEST_DEP_VERSION"]
            == "1.2.3"
        )


class TestResolver:
    def test_sorted_order_raises_exception_if_cyclic_deps(self, build_definition):
        build_definition.add_module(ModuleSource("a", versions=[], dependencies=["b"]))
        build_definition.add_module(ModuleSource("b", versions=[], dependencies=["a"]))

        with pytest.raises(DependencyResolutionError):
            Resolver().get_sorted_order(build_definition)

    def test_sorted_order(self, build_definition):
        build_definition.add_module(ModuleSource("a", versions=[]))
        build_definition.add_module(ModuleSource("b", versions=[], dependencies=["a"]))

        assert Resolver().get_sorted_order(build_definition) == [
            "base",
            "require",
            "a",
            "b",
        ]

    def test_sorted_order_missing_dependency(self, build_definition):
        build_definition.add_module(ModuleSource("a", versions=[], dependencies=["b"]))

        with pytest.raises(DependencyResolutionError):
            assert Resolver().get_sorted_order(build_definition)

    def test_sorted_order_allow_missing_dependency(self, build_definition):
        build_definition.add_module(ModuleSource("a", versions=[], dependencies=["b"]))

        assert Resolver(ignore_dependencies=True).get_sorted_order(
            build_definition
        ) == [
            "base",
            "require",
            "a",
        ]
