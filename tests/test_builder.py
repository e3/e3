import subprocess
from pathlib import Path
from unittest.mock import patch

import pytest
from git import Repo

from e3.builder import Builder
from e3.fs.environment import InstalledEnvironment
from e3.module import EPICSBaseSource, ModuleSource, RequireSource


@pytest.fixture
def modulesource_with_multiple_versions(tmp_path):
    module_name = "foo"
    module_versions = ["0.0.1+0", "0.1.0+1", "1.0.0+1"]
    module = ModuleSource(module_name, versions=module_versions)
    for version in module_versions:
        module.versions[version]["version_string"] = version

    module_path = tmp_path / f"e3-{module_name}"
    module_path.mkdir()

    repo = Repo.init(module_path)
    repo.index.commit("Initial commit")
    module.repo = repo
    for version in module_versions:
        repo.create_tag(version)

    yield module


class TestBuilder:
    @pytest.mark.parametrize(
        "module",
        [
            EPICSBaseSource(version=""),
            RequireSource(version=""),
            ModuleSource("", versions=[""]),
        ],
        ids=["base", "require", "module"],
    )
    def test_modify_install_path(self, tmp_path, module):
        config_dir = tmp_path / "configure"
        config_dir.mkdir()

        config_data = f"""
FOO:=bar
FOO2=blip
FOO3?=bing
{module.install_config_var}:=qux
"""

        install_config_path = tmp_path / module.install_config_file
        install_config_path.write_text(config_data)

        Builder.modify_install_path(
            module, repo_path=tmp_path, install_path=Path("baz")
        )

        assert "baz" in install_config_path.read_text()
        assert "qux" not in install_config_path.read_text()

    @patch("e3.builder.Builder.modify_install_path", return_value=None)
    def test_build_fails_for_invalid_url(
        self, mock_modify_install_path, tmp_path: Path
    ):
        base_version = "foo"
        test_environment = InstalledEnvironment(tmp_path, base_version, "")
        builder = Builder(build_dir=tmp_path)

        base = EPICSBaseSource(version=base_version)
        base._url = ""
        base.versions[base_version]["version_string"] = base_version

        module_path = tmp_path / f"e3-{base.name}"
        module_path.mkdir()
        repo = Repo.init(module_path)
        repo.index.commit("Initial commit")
        repo.create_tag(base_version)

        successes, failures = builder.build(base, test_environment)
        assert not successes
        assert failures

    def test_build_returns_failure_if_module_not_initialised(self, tmp_path: Path):
        builder = Builder(tmp_path)
        base_version = "foo"
        target_environment = InstalledEnvironment(tmp_path, base_version, "")
        module = ModuleSource("foo", versions=["bar"])

        successes, failures = builder.build(module, target_environment)
        assert not successes
        assert failures

    @patch(
        "e3.utils.run_make", return_value=subprocess.CompletedProcess([], returncode=0)
    )
    @patch("e3.builder.Builder.modify_install_path", return_value=None)
    def test_build_return_success_if_module_has_many_versions(
        self,
        mock_modify_install_path,
        mock_run_make,
        tmp_path: Path,
        modulesource_with_multiple_versions,
    ):
        test_environment = InstalledEnvironment(tmp_path, "", "")
        builder = Builder(build_dir=tmp_path)
        module = modulesource_with_multiple_versions

        with patch.object(Builder, "_clone_repo", return_value=module.repo):
            successes, failures = builder.build(module, test_environment)
            assert successes
            assert not failures

    @patch("e3.builder.utils.run_make")
    @patch("e3.builder.Builder.modify_install_path", return_value=None)
    def test_build_return_failure_if_module_has_many_versions_and_build_fail(
        self,
        mock_modify_install_path,
        mock_run_make,
        tmp_path: Path,
        modulesource_with_multiple_versions,
    ):
        test_environment = InstalledEnvironment(tmp_path, "", "")
        builder = Builder(build_dir=tmp_path)
        module = modulesource_with_multiple_versions

        result_fail = subprocess.CompletedProcess([], returncode=1)
        result_success = subprocess.CompletedProcess([], returncode=0)

        # Process calls are: clone, init, patch, build, install
        first_version_results = [result_success] * 5
        second_version_results = [result_success] * 3 + [result_fail] + [result_success]
        third_version_results = [result_success] * 5
        mock_run_make.side_effect = (
            first_version_results + second_version_results + third_version_results
        )
        with patch.object(Builder, "_clone_repo", return_value=module.repo):
            successes, failures = builder.build(module, test_environment)
            # As we fail fast only the first version will succeed, the third
            # version will never be build.
            assert len(successes) == 1
            assert len(failures) == 1
