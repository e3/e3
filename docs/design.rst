===============================
e3 Build Manager Design
===============================

The purpose of the e3 Build Manager is to provide the ability to generate
reproducible, consistent e3 environments, as well as to update an existing e3
environment. This will allow maintainers and users to ensure that they can
reproduce an earlier state of a given system.

------------------------------
Software Design Considerations
------------------------------

Our main considerations in designing the build manager is that it should
produce e3 environments that are

* repeatable: we should be able to easily share an e3 environment with
  another person or organisation, as well as be able to reproduce one
  ourselves if needed.
* updateable: we should be able to add new modules to an existing environment
  without being required to build a completely new environment

We have chosen to implement an interactive command-line utility, which is
simpler to use and install, and fits naturally into CI pipelines.

------
Inputs
------

The input to the specification handler is a `specification` file. This is a
YAML file that describes an e3 environment. An example is the following:

.. code-block::

  metadata:
    type: "specification"
    version: 1
  config:
    base: "foo"
    require: "bar"
  modules:
    iocstats:
      versions:
      - "baz"
      - "qux"

This describes an environment with EPICS base built with the tag
``foo``, as well as ``require`` (with the tag ``bar``) and the e3-module
``iocstats`` (with both versions defined by the tags ``baz`` and ``qux``).

------------
Dependencies
------------

By design, the specification handler depends on having read-access to the
ESS GitLab instance hosted at ``https://gitlab.esss.lu.se``.

---------------------
Software Architecture
---------------------

The architecture of the specification handler is as follows.

.. image:: img/build-arch.png

Specifically, the build process performs the following steps.

1. Fetch: Fetch the data from GitLab in order to determine exactly what should
   be built based on the individual module configurations
2. Curate: Compare the requested environment to the existing environment in
   order determine which module versions need to be built
3. Resolve: Determine the build order for the modules
4. Build: Build the module versions that remain to be built
