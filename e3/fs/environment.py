"""Core objects for interacting with existing e3 installations."""

import logging
from pathlib import Path
from string import Template

BASE_DIR = Template("base-$base_version")
REQUIRE_DIR = "require"
MODULES_DIR = "siteMods"

logger = logging.getLogger(__name__)


class InstalledEnvironment:
    """Class to represent installed e3 environments."""

    def __init__(self, root: Path, base_version: str, require_version: str) -> None:
        """Initialize object."""
        self.root = root
        self.base_version = base_version
        self.require_version = require_version
        self.base_directory = self.root / BASE_DIR.substitute(
            base_version=self.base_version
        )
        self.require_directory = (
            self.base_directory / REQUIRE_DIR / self.require_version
        )
        self.modules_directory = self.require_directory / MODULES_DIR

    def __repr__(self) -> str:
        """Return serialized object."""
        return f"{self.__class__.__name__}({self.root!r}, {self.base_version!r}, {self.require_version!r})"

    def has(self, module_name: str, version: str) -> bool:
        """Return whether the module version is installed.

        Relies on a directory check for EPICS base, and otherwise checks if the meta file exists.
        This is done so because EPICS base did not create and install meta files for the environments we support.
        """
        if module_name == "base":
            assert version == self.base_version
            path = self.base_directory
        elif module_name == "require":
            assert version == self.require_version
            path = self.require_directory / f"{module_name}_meta.yaml"
        else:
            path = (
                self.modules_directory
                / module_name
                / version
                / f"{module_name}_meta.yaml"
            )
        exists = path.exists()
        if exists:
            logger.debug(f"{module_name!r}, {version!r} detected at {path!s}")
        return exists
