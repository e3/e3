"""Module for custom exceptions."""


class DependencyResolutionError(Exception):
    """The dependencies cannot be resolved."""


class ModuleExistsException(Exception):
    """The given module has already been added to the environment."""


class MissingRemoteException(Exception):
    """No remote is defined."""


class NotInitialisedError(Exception):
    """The URL to the module has not yet been retrieved."""


class InvalidVersionException(Exception):
    """The version is not with a valid pattern."""


class FetchDataException(Exception):
    """Was not able to fetch data from registry."""


class ProcessException(Exception):
    """Process step failed."""


class ModuleBuildFailException(Exception):
    """Module build failed."""


class NoModuleChangesException(Exception):
    """No changes to be commited."""


class MissingReferenceException(Exception):
    """Reference was not found on repository."""


class FileNotFoundException(Exception):
    """Fine was not found on repository."""
