"""Entry-point to build an e3 environment from a specification."""

import argparse
import logging
import sys
from pathlib import Path

from e3.exceptions import ProcessException
from e3.process import EnvironmentBuildProcess

logger = logging.getLogger(__name__)


def build_e3(
    specification: Path,
    build_dir: Path,
    install_path: Path,
    use_ssh: bool,
    assume_yes: bool,
    token: str,
    verbose: bool,
    log_file: Path,
    jobs: int,
) -> None:
    """Build an e3-tree from a specification."""
    process = EnvironmentBuildProcess(
        specification,
        build_dir,
        install_path,
        use_ssh,
        token,
        verbose,
        log_file,
        jobs,
    )
    try:
        process.setup()

        if not assume_yes:
            answer = input("Do you want to continue? ")
            if answer.lower() not in ("y", "ye", "yes"):
                logger.info("Exiting.")
                sys.exit(-1)

        process.fetch()
        process.curate()
        process.resolve()
        process.build()
    except ProcessException:
        logger.critical("Process failed - aborting.")
        sys.exit(-1)


def main():
    """Run the main function."""
    parser = argparse.ArgumentParser()

    parser.add_argument("specification", type=Path, help="path to specification")
    parser.add_argument(
        "-b",
        "--build-dir",
        type=Path,
        default=Path("build"),
        help="path to build location",
    )
    parser.add_argument(
        "-t",
        "--install-path",
        type=Path,
        default=Path("/epics"),
        help="path to install location",
    )
    parser.add_argument(
        "--use-ssh",
        action="store_true",
        help="clone repositories using ssh",
    )
    parser.add_argument(
        "-y",
        "--assume-yes",
        action="store_true",
        help="automatically answer yes for all questions",
    )
    parser.add_argument("--token", help="private token to connect to Gitlab")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--log-file", type=Path, help="destination log file")
    parser.add_argument(
        "-j",
        "--jobs",
        type=int,
        default=1,
        help="number of parallel build jobs (use only with environments containing EPICS base > 7.0.8.1)",
    )

    args = parser.parse_args()
    build_e3(**vars(args))
