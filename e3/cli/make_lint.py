"""Entry-point to linting the makefile on the e3 modules."""

import argparse
import logging
import sys
from pathlib import Path

from e3.fs.make import find_cxx_stds, find_undefined_vars
from e3.logging import set_up_logger
from e3.utils import WrapperType, get_wrapper_type

logger = logging.getLogger(__name__)


def lint_makefile(wrapper: Path, debug: bool) -> None:
    """Lint e3 makefiles.

    Raises:
        TypeError: if wrapper is not a valid e3 wrapper.
    """
    set_up_logger(debug)

    if get_wrapper_type(wrapper) == WrapperType.BASE:
        logger.error("Please don't try to lint EPICS base.")
        sys.exit(-1)

    issue_detected = False
    undefined = find_undefined_vars(wrapper)
    if undefined:
        logger.error(f"You have undefined vars: {undefined}")
        issue_detected = True
    stds = find_cxx_stds(wrapper)
    if stds:
        logger.error(f"Don't define std on a module level - you have defined {stds}")
        issue_detected = True

    if issue_detected:
        sys.exit(-1)
    logger.info("No issues detected.")


def main():
    """Run the main function."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "wrapper",
        nargs="?",
        type=Path,
        default=Path.cwd(),
        help="Path to wrapper repository",
    )
    parser.add_argument("-d", "--debug", action="store_true", help=argparse.SUPPRESS)

    args = parser.parse_args()

    lint_makefile(**vars(args))
