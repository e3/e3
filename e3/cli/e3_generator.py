"""Entry-point to generate specifications."""

import argparse
import logging
import sys
from datetime import datetime
from pathlib import Path

from e3.exceptions import ProcessException
from e3.process import SpecificationGenerationProcess

logger = logging.getLogger(__name__)


def generate_e3(
    formula: Path,
    build_dir: Path,
    install_path: Path,
    use_ssh: bool,
    group_id: int,
    assume_yes: bool,
    token: str,
    verbose: bool,
    log_file: Path,
    dry_run: bool,
    branch: str,
) -> None:
    """Generate a specification file based on a formula."""
    process = SpecificationGenerationProcess(
        formula,
        build_dir,
        install_path,
        use_ssh,
        group_id,
        token,
        verbose,
        log_file,
        branch,
    )
    try:
        process.setup()

        if not assume_yes:
            answer = input("Do you want to continue? ")
            if answer.lower() not in ("y", "ye", "yes"):
                logger.info("Exiting.")
                sys.exit(-1)

        process.fetch()
        process.curate()
        process.resolve()
        process.build()

        if not dry_run:
            process.teardown()

        process.generate(dry_run)

    except ProcessException:
        logger.critical("Process failed - aborting.")
        sys.exit(-1)


def main():
    """Run the main function."""
    parser = argparse.ArgumentParser()

    parser.add_argument("formula", type=Path, help="path to formula")
    parser.add_argument(
        "-b",
        "--build-dir",
        type=Path,
        default=Path("build"),
        help="path to build location",
    )
    parser.add_argument(
        "-t",
        "--install-path",
        type=Path,
        default=Path("/tmp/epics") / datetime.now().strftime("%Y%m%d-%H%M%S"),
        help=argparse.SUPPRESS,
    )
    parser.add_argument(
        "--use-ssh",
        action="store_true",
        help="clone repositories using ssh",
    )
    parser.add_argument("--group-id", type=int, help=argparse.SUPPRESS)
    parser.add_argument(
        "-y",
        "--assume-yes",
        action="store_true",
        help="automatically answer yes for all questions",
    )
    parser.add_argument("--token", help="private token to connect to Gitlab")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--log-file", type=Path, help="destination log file")
    parser.add_argument(
        "-n",
        "--dry-run",
        action="store_true",
        help="do not perform any git actions (commit, tag) and do not generate specification",
    )
    parser.add_argument(
        "-r",
        "--branch",
        help="create module changes commits on the specificied branch. Branch is used on the specification ",
        default=None,
    )

    args = parser.parse_args()
    generate_e3(**vars(args))
