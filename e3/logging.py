"""Module for log configuration."""

import logging
import logging.config
from pathlib import Path
from typing import Any, Dict, Union, Text, Optional

logger = logging.getLogger(__name__)

HIGHLIGHT_LVL = 25


class CustomLogger(logging.Logger):
    """Custom logger type."""

    def __init__(self, name: str, level: Union[int, Text] = logging.NOTSET) -> None:
        super().__init__(name, level)
        logging.addLevelName(HIGHLIGHT_LVL, "HIGHLIGHT")

    def highlight(self, msg: str, *args: Any, **kwargs: Any) -> None:
        if self.isEnabledFor(HIGHLIGHT_LVL):
            self._log(HIGHLIGHT_LVL, msg, args, **kwargs)


class ConsoleFormatter(logging.Formatter):
    """Custom console formatter."""

    formats = {
        "standard": "%(message)s",
        "highlight": ":: %(message)s",
    }

    def format(self, record: logging.LogRecord) -> str:
        """Set the correct format style for each record."""
        if record.levelno == HIGHLIGHT_LVL:
            self._style._fmt = self.formats["highlight"]
        else:
            self._style._fmt = self.formats["standard"]
        return logging.Formatter.format(self, record)


def set_up_logger(verbose: bool, log_file: Optional[Path] = None) -> None:
    """Set up the log environment."""
    config: Dict[str, Any] = {
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "verbose": {
                "style": "{",
                "format": "{asctime} [{levelname:^9}] {name}: {message}",
            },
            "consoleFormatter": {"class": "e3.logging.ConsoleFormatter"},
        },
        "handlers": {
            "console": {
                "level": "INFO",
                "formatter": "consoleFormatter",
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",
            },
        },
        "loggers": {
            "e3": {"handlers": ["console"], "level": "DEBUG", "propagate": True},
        },
    }
    if verbose:
        config["handlers"]["console"].update({"formatter": "verbose", "level": "DEBUG"})
    if log_file:
        config["handlers"]["file"] = {
            "level": "DEBUG",
            "formatter": "verbose",
            "class": "logging.FileHandler",
            "filename": log_file,
        }
        config["loggers"]["e3"]["handlers"].append("file")

    logging.config.dictConfig(config)


def pretty_log(key: str, val: str) -> None:
    """Log formated string using the standard logger."""
    logger.info(f"{key:<25} = {val}")
