"""Module for functions relating to generating remote tag."""

import logging
import sys
from typing import Tuple

from e3 import utils
from e3.exceptions import InvalidVersionException
from e3.git.registry import WrapperRegistry
from e3.git.tag import Tag
from e3.module import EPICSBaseSource, ModuleSource
from e3.exceptions import MissingReferenceException, FileNotFoundException

logger = logging.getLogger(__name__)


def generate_remote_tag(registry: WrapperRegistry, module: str, ref: str) -> str:
    """Generate and return a tag for an e3 wrapper remote repository."""
    try:
        base_ver, require_ver, module_ver, commit_hash = read_remote_versions(
            registry, module, ref
        )
    except InvalidVersionException as e:
        logger.error(str(e))
        sys.exit(-1)
    except MissingReferenceException:
        logger.error(
            "Reference not found. Please make sure that the branch, tag or commit exists in the repository."
        )
        sys.exit(-1)
    except FileNotFoundException:
        logger.error(
            "File not found. The configure/CONFIG file is missing from the repository."
        )
        sys.exit(-1)
    except ValueError:
        logger.error("Remote module has an invalid definition of EPICS_BASE.")
        sys.exit(-1)

    return Tag.from_components(base_ver, require_ver, module_ver, commit_hash).name


def read_remote_versions(
    registry: WrapperRegistry, module: str, ref: str
) -> Tuple[str, str, str, str]:
    """Return a tuple containing various config data for a remote wrapper.

    Raises:
        InvalidVersionException if versions are not in the expected pattern.
        FileNotFoundException: if configure/RELEASE is not found on the repository.
        MissingReferenceException: if reference is missing from the repository.
        ValueError: if remote repository does not have a valid EPICS base configuration.
    """
    if module != "base":
        base_ver = utils.extract_base_version(
            registry.read_version_from(
                module,
                ref,
                config="configure/RELEASE",
                var="EPICS_BASE",
                validate=False,
            )
        )
        require_ver = registry.read_version_from(
            module,
            ref,
            config="configure/RELEASE",
            var="E3_REQUIRE_VERSION",
            validate=False,
        )
    else:
        base_ver = registry.read_version_from(
            module,
            ref,
            config=EPICSBaseSource.version_config_file,
            var=EPICSBaseSource.version_config_var,
            validate=False,
        )
        require_ver = "NA"

    if module == "require":
        module_ver = require_ver
    elif module == "base":
        module_ver = base_ver
    else:
        module_ver = registry.read_version_from(
            module,
            ref,
            config=ModuleSource.version_config_file,
            var=ModuleSource.version_config_var,
            validate=False,
        )

    commit_hash = registry.get_commit_hash(module, ref)

    if module != "base":
        if module != "require" and not utils.check_module_version(module_ver):
            raise InvalidVersionException(f"Module version {module_ver} is invalid")
        if not utils.check_require_version(require_ver):
            raise InvalidVersionException(f"Require version {require_ver} is invalid")

    if not utils.check_base_version(base_ver):
        raise InvalidVersionException(f"Base version {base_ver} is invalid")

    return base_ver, require_ver, module_ver, commit_hash
