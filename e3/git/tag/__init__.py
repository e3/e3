"""Module for functions relating to git and e3 tags."""

import logging
from datetime import datetime

from e3.logging import pretty_log

logger = logging.getLogger(__name__)


class Tag:
    """Class for e3 tags."""

    def __init__(self, name: str) -> None:
        self.name = name

    def __str__(self) -> str:
        return self.name

    @classmethod
    def from_components(
        cls, base_ver: str, require_ver: str, module_ver: str, commit_sha: str
    ) -> "Tag":
        """Create and return an e3 tag from its components."""
        datestamp = datetime.now().strftime("%Y%m%d")
        timestamp = datetime.now().strftime("%H%M%S")

        # Note that the commit_sha is both truncated and padded to exactly 7 chars
        tag = f"{base_ver}-{require_ver}/{module_ver}-{commit_sha:7.7}-{datestamp:8}T{timestamp:6}"

        output = {
            "Base version": base_ver,
            "Require version": require_ver,
            "Module version": module_ver,
            "Git commit": commit_sha,
        }
        for key, val in output.items():
            pretty_log(key, val)

        logger.debug(f"Composing new tag {tag}")
        return cls(tag)
