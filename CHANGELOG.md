# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Add `-j` option to `e3-build` to specify the number of parallel `make` jobs

## [3.0.15] - 2024-10-09

- Improve `e3-release` logging readability

## [3.0.14] - 2024-10-01

- Update default env in `e3-release` to 2024q3

## [3.0.13] - 2024-09-04

- Add `assume-yes` option to `e3-release` in lieu of `dry-run`

## [3.0.12] - 2024-08-08

- Fix prefix lookup for `e3-release` (allow both `module_name` and `e3-module_name`)

## [3.0.11] - 2024-07-19

- Minor API changes in `e3.git.registry`

## [3.0.10] - 2024-07-18

- Minor improvements to `e3-release`

## [3.0.9] - 2024-07-17

- Minor API changes in `e3.git.registry`

## [3.0.8] - 2024-06-18

- Modify automatic reviewers for `e3-release`

## [3.0.7] - 2024-05-21

- Improved logging output for `e3-builder` and `e3-generator`

## [3.0.6] - 2024-05-08

- Bugfix for `e3-generator`

## [3.0.5] - 2024-05-07

- Bugfix for `e3-release`

## [3.0.4] - 2024-04-29

- Minor feedback improvements for `e3-release`

## [3.0.3] - 2024-04-23

- Bugfix for `e3-release`

## [3.0.2] - 2024-04-12

- Bugfix for `e3-release`

## [3.0.1] - 2024-04-11

- Minor adjustments to `e3-release`

## [3.0.0] - 2024-04-10

- Adds `e3-release`, a CLI utility to simplify the module release workflow
- Removes `e3-tag-local` and `e3-tag-remote`.

## [2.1.0] - 2023-07-12

- Removed tag checking from `e3-tag-local`.

## [2.0.0] - 2022-08-12

This is a major release since the entire API has been modified. Most changes
have been implemented in order to make use of this package as deployment mechanism
for ESS' EPICS releases. Some changes relate to an utility for administrators to
use when creating new EPICS releases.

- Adds `e3-generate` for generating specifications

## [1.1.0] - 2022-04-29

- `tag-e3-wrapper` has been split into `e3-tag-local` and `e3-tag-remote`
- Cleaned up old bash tools that are no longer needed due to the build manager

## [1.0.0] - 2022-04-06

- Added completely new way to build e3 environments, `e3-build`. See [the documentation](docs/README_specification.md).
- Replaced `git-python` dependency with the correct `GitPython`
- Updated specification inline documentation - docstrings and comments
- Added progress logger for build progress reporting
- Added ability to tag a remote repository in `tag-e3-wrapper`

## [0.4.1] - 2021-06-02

- Refactored python utilities
- Restructured `.gitlab-ci.yml` to match standard check, build, test, release
  structure

## [0.4.0]

- Added linter for e3 makefiles, `lint-makefile`, also installed on path with
  `pip3 install e3`.
- Updated logging for `tag-e3-wrapper`
- Added `snmp` module to `communication` group

## [0.3.0]

- Replaced `tag-e3-wrapper` with `tag.py` and added CLI entrypoint... called
  `tag-e3-wrapper`
- Added tests for tagging utility
- Updated inventory to reflect `common` -> `core`, `communication` split
- Added `modulator` and `rs_nrpz` to `rf` group

## [0.2.0]

With this release, `e3` also becomes a python package, installable using pip.
When installing using this method, python modules are installed, and some
utilities are appended to path. The first such utility is `tag-e3-wrapper`.

- Started migration to using python for many utilities
- Installable using `pip`, includes `tag-e3-wrapper` utility
- Added an option for `tag-e3-wrapper` to check that the tag matches the current
  config
- Removed a lot of legacy files
- Major refactor and simplification of `e3.bash`
- When cloning a repository with e3.bash (e.g. `bash e3.bash -c gmod)` or `bash
  e3.bash greq`, etc), the latest tagged commit will be checked out if it
  exists, instead of the master branch. This allows for more consistent
  replication in case of new changes in the wrapper repositories.
- Replaced `e3-inventory.txt` with `e3-inventory.yaml`, which contains some
  metadata regarding dependencies.  Also added `get_inventory.py` to parse this
  and return information about groups and modules within those groups.
- Added Ubuntu 18.04 to gitlab CI build
- Improved gitlab-CI, including that it now also builds and installs e3-autosave
  as well as the "common" module group
- Updated default base/require to 7.0.5/3.4.0 in `e3_building_config.bash`
- Changes `tools/protect_branches.py` to `tools/protect_repos.py`; now also
  protects a tag

## [0.1.0] - 2021-01-12

- First tagged version of this repository
- Incorporates all changes made up to this date
- See https://jira.esss.lu.se/browse/E3-26 for more information

[Unreleased]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.15...HEAD
[3.0.15]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.14...3.0.15
[3.0.14]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.13...3.0.14
[3.0.13]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.12...3.0.13
[3.0.12]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.11...3.0.12
[3.0.11]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.10...3.0.11
[3.0.10]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.9...3.0.10
[3.0.9]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.8...3.0.9
[3.0.8]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.7...3.0.8
[3.0.7]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.6...3.0.7
[3.0.6]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.5...3.0.6
[3.0.5]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.4...3.0.5
[3.0.4]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.3...3.0.4
[3.0.3]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.2...3.0.3
[3.0.2]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.1...3.0.2
[3.0.1]: https://gitlab.esss.lu.se/e3/e3/compare/3.0.0...3.0.1
[3.0.0]: https://gitlab.esss.lu.se/e3/e3/compare/2.1.0...3.0.0
[2.1.0]: https://gitlab.esss.lu.se/e3/e3/compare/2.0.0...2.1.0
[2.0.0]: https://gitlab.esss.lu.se/e3/e3/compare/1.1.0...2.0.0
[1.1.0]: https://gitlab.esss.lu.se/e3/e3/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.esss.lu.se/e3/e3/compare/0.4.1...1.0.0
[0.4.1]: https://gitlab.esss.lu.se/e3/e3/compare/0.1.0...0.4.1
[0.4.0]: https://gitlab.esss.lu.se/e3/e3/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.esss.lu.se/e3/e3/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.esss.lu.se/e3/e3/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.esss.lu.se/e3/e3/-/tags/0.1.0
